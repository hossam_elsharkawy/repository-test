package com.hossamelsharkawy.test.modules

import com.hossamelsharkawy.req.Res
import com.hossamelsharkawy.req.ResData
import com.hossamelsharkawy.req.Server
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface CarApi {

    companion object {
      val ini by lazy {Server.create(CarApi::class.java)  }
    }

    @GET("car/car_list")
    fun getAllCar(): Deferred<ResData<ArrayList<Car>>>
    
    @GET("car/car_list")
    fun getOneCar(@Query("id") id: Int): Deferred<ResData<Car>>
    
    @FormUrlEncoded
    @POST("car/update_car/{id}")
    fun updateCar(@Path("id") id: String,
                    @FieldMap mMap: ArrayMap<String, String> = map): Deferred<Res>
                    
   @FormUrlEncoded
    @POST("car/delete_car")
    fun deleteGroup(@Field("id") id: Int): Deferred<Res>
                    
    @FormUrlEncoded
    @POST("car/add_new_car")
    fun addNewCar(@FieldMap mMap: ArrayMap<String, String> = map): Deferred<Res>
                    
}

