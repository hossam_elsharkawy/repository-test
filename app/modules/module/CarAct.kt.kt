package com.hossamelsharkawy.test.modules.car

import android.content.Context
import android.content.Intent
import com.elsharkawe.base.ui.act.Act
import com.hossamelsharkawy.test.R


class CarAct(override var layout: Int = R.layout.car_act) : Act() {

  companion object {
        fun start(context: Context) = context.startActivity(Intent(context, CarAct::class.java))
    }
    
     override fun onView() {
   
    }

}