package com.hossamelsharkawy.test.modules.meeting

import android.content.Context
import android.content.Intent
import com.elsharkawe.base.ui.act.Act
import com.hossamelsharkawy.test.R


class MeetingAct(override var layout: Int = R.layout.meeting_act) : Act() {

  companion object {
        fun start(context: Context) = context.startActivity(Intent(context, MeetingAct::class.java))
    }
    
     override fun onView() {
   
    }

}