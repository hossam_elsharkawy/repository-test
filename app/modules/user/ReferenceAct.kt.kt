package com.get.aa.modules.reference

import android.content.Context
import android.content.Intent
import com.elsharkawe.base.ui.act.Act
import com.get.aa.R


class ReferenceAct(override var layout: Int = R.layout.reference_act) : Act() {

  companion object {
        fun start(context: Context) = context.startActivity(Intent(context, ReferenceAct::class.java))
    }
    
     override fun onView() {
   
    }

}