package com.hossamelsharkawy.test.modules.route

import android.content.Context
import android.content.Intent
import com.elsharkawe.base.ui.act.Act
import com.hossamelsharkawy.test.R


class RouteAct(override var layout: Int = R.layout.route_act) : Act() {

  companion object {
        fun start(context: Context) = context.startActivity(Intent(context, RouteAct::class.java))
    }
    
     override fun onView() {
   
    }

}