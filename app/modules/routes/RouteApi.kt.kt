package com.hossamelsharkawy.test.modules

import com.hossamelsharkawy.req.Res
import com.hossamelsharkawy.req.ResData
import com.hossamelsharkawy.req.Server
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface RouteApi {

    companion object {
      val ini by lazy {Server.create(RouteApi::class.java)  }
    }

    @GET("route/route_list")
    fun getAllRoute(): Deferred<ResData<ArrayList<Route>>>
    
    @GET("route/route_list")
    fun getOneRoute(@Query("id") id: Int): Deferred<ResData<Route>>
    
    @FormUrlEncoded
    @POST("route/update_route/{id}")
    fun updateRoute(@Path("id") id: String,
                    @FieldMap mMap: ArrayMap<String, String> = map): Deferred<Res>
                    
   @FormUrlEncoded
    @POST("route/delete_route")
    fun deleteGroup(@Field("id") id: Int): Deferred<Res>
                    
    @FormUrlEncoded
    @POST("route/add_new_route")
    fun addNewRoute(@FieldMap mMap: ArrayMap<String, String> = map): Deferred<Res>
                    
}

